=== Customer Email verification for WooCommerce   ===
Contributors: zorem
Tags: woocommerce, email verification, email validation, customer account
Requires at least: 4.0
Requires PHP: 5.2.4
Tested up to: 5.4
Stable tag: 5.3
License: GPLv2 
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This Customer Email verification will reduce WooCommerce registration spam in your store and will require them to verify their email address by sending a verification link to the email address which they registered to their account. You can allow the customer to log-in to their account when they first register and to require them to verify the email on the next login or you can restrict access to their account until they verify their email.

== Key Features==

* Require email verification for new user registrations
* Option to add the verification link in the WooCommerce new account email or to send the verification in a separate email
* Option to re-send the verification link on login form in cases that the customer lost the first verification email.
* Customize the separate email verification header/subject/message
* Option to customize the verification message
* Option to allow the customer to enter his account after the first time he registered without email verification.
* Skip email verification for selected user roles
* Customize the frontend messages
* Redirect users to any page on your website after successful validation
* Option for manual email verification from admin
* Email verification status will display for each user on admin

== Installation ==

1. Upload the folder `customer-email-verification-for-woocommerce` to the `/wp-content/plugins/` folder
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0.2 =
* Tested with WordPress 5.4 and WooCommerce 4.0

= 1.0.1 =
* Fixed issue with WooCommerce email
* Fixed skip email verification for selected roles option save issue
* Fixed warnings from users list page

= 1.0 =
* Initial version.
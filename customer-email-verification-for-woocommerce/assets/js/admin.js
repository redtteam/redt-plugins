( function( $, data, wp, ajaxurl ) {
		
	var $cev_settings_form = $("#cev_settings_form");
	var $cev_frontend_messages_form = $("#cev_frontend_messages_form");	
			
	var cev_settings_init = {
		
		init: function() {									
			$cev_settings_form.on( 'click', '.cev_settings_save', this.save_wc_cev_settings_form );
			$cev_frontend_messages_form.on( 'click', '.cev_frontend_messages_save', this.save_wc_cev_frontend_messages_form );			
		},

		save_wc_cev_settings_form: function( event ) {
			
			event.preventDefault();
			
			$cev_settings_form.find(".spinner").addClass("active");
			var ajax_data = $cev_settings_form.serialize();
			
			$.post( ajaxurl, ajax_data, function(response) {
				$cev_settings_form.find(".spinner").removeClass("active");
			});
			
		}, 
		save_wc_cev_frontend_messages_form: function( event ) {
			
			event.preventDefault();
			
			$cev_frontend_messages_form.find(".spinner").addClass("active");
			var ajax_data = $cev_frontend_messages_form.serialize();
			
			$.post( ajaxurl, ajax_data, function(response) {
				$cev_frontend_messages_form.find(".spinner").removeClass("active");
			});
			
		}
	};
	
	$(window).load(function(e) {
        cev_settings_init.init();
    });	
	
})( jQuery, customer_email_verification_script, wp, ajaxurl );

jQuery(document).on("change", ".cev_email_for_verification", function(){	
	var cev_email_for_verification = jQuery(this).val();
	if(cev_email_for_verification == 1){
		jQuery('.cev_verification_email_customizer_tr').show();
		jQuery('.cev_include_verification_email_body_tr').hide();
		jQuery('.cev_include_verification_email_body_tags_tr').hide();
	} else{
		jQuery('.cev_verification_email_customizer_tr').hide();
		jQuery('.cev_include_verification_email_body_tr').show();
		jQuery('.cev_include_verification_email_body_tags_tr').show();		
	}
});
jQuery( document ).ready(function() {	
	var cev_email_for_verification = jQuery('.cev_email_for_verification').val();
	if(cev_email_for_verification == 1){
		jQuery('.cev_verification_email_customizer_tr').show();
		jQuery('.cev_include_verification_email_body_tr').hide();
		jQuery('.cev_include_verification_email_body_tags_tr').hide();
	} else{
		jQuery('.cev_verification_email_customizer_tr').hide();
		jQuery('.cev_include_verification_email_body_tr').show();
		jQuery('.cev_include_verification_email_body_tags_tr').show();
	}
});
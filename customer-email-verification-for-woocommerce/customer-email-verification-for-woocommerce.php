<?php
/**
 * @wordpress-plugin
 * Plugin Name: Customer Email verification for WooCommerce 
 * Plugin URI:  
 * Description: This plugin Customer Email verification for WooCommerce will reduce registration spam in your store and verifies the email address of the customers by sending a verification link to the email address that they registered to their accounts. You can allow the customer to be able to log in to their account when they registered and to require them to verify the email for next logins or restrict access the my-account area to users that verified their accounts.
 * Version: 1.0.2
 * Author: zorem
 * Author URI:  
 * License: GPL-2.0+
 * License URI: 
 * Text Domain: customer-email-verification-for-woocommerce
 * Domain Path: /lang/
 * WC tested up to: 4.0
*/


class zorem_woo_customer_email_verification {
	/**
	 * Customer Email verification for WooCommerce version.
	 *
	 * @var string
	 */
	public $version = '1.0.2';
	
	/**
	 * Initialize the main plugin function
	*/
    public function __construct() {
		
		$this->plugin_file = __FILE__;
		// Add your templates to this array.
		
		if(!defined('CUSTOMER_EMAIL_VERIFICATION_PATH')) define( 'CUSTOMER_EMAIL_VERIFICATION_PATH', $this->get_plugin_path());							
			
		
		if ( $this->is_wc_active() ) {
			
			// Include required files.
			$this->includes();			
			
			//start adding hooks
			$this->init();

			$this->admin->init();	
			
			$this->email->init();
			
			add_action( 'plugins_loaded', array( $this, 'on_plugins_loaded' ) );
		}		
    }
	
	/**
	 * Check if WooCommerce is active
	 *
	 * @access private
	 * @since  1.0.0
	 * @return bool
	*/
	private function is_wc_active() {
		
		if ( ! function_exists( 'is_plugin_active' ) ) {
			require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
		}
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			$is_active = true;
		} else {
			$is_active = false;
		}
		

		// Do the WC active check
		if ( false === $is_active ) {
			add_action( 'admin_notices', array( $this, 'notice_activate_wc' ) );
		}		
		return $is_active;
	}
	
	/**
	 * Display WC active notice
	 *
	 * @access public
	 * @since  1.0.0
	*/
	public function notice_activate_wc() {
		?>
		<div class="error">
			<p><?php printf( __( 'Please install and activate WooCommerce!', '' ), '<a href="' . admin_url( 'plugin-install.php?tab=search&s=WooCommerce&plugin-search-input=Search+Plugins' ) . '">', '</a>' ); ?></p>
		</div>
		<?php
	}
	
	/**
	 * Gets the absolute plugin path without a trailing slash, e.g.
	 * /path/to/wp-content/plugins/plugin-directory.
	 *
	 * @return string plugin path
	 */
	public function get_plugin_path() {
		if ( isset( $this->plugin_path ) ) {
			return $this->plugin_path;
		}

		$this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );

		return $this->plugin_path;
	}
	
	/**
	 * Gets the absolute plugin url.
	 */	
	public function plugin_dir_url(){
		return plugin_dir_url( __FILE__ );
	}
	
	/*
	* init when class loaded
	*/
	public function init(){
		add_action( 'plugins_loaded', array( $this, 'customer_email_verification_load_textdomain'));

		//Custom Woocomerce menu
		add_action('admin_menu', array( $this->admin, 'register_woocommerce_menu' ), 99 );

		//load css js 
		add_action( 'admin_enqueue_scripts', array( $this->admin, 'admin_styles' ), 4);	
	}
	
	/*** Method load Language file ***/
	function customer_email_verification_load_textdomain() {
		load_plugin_textdomain( 'customer-email-verification-for-woocommerce', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
	}
	
	/*
	* include files
	*/
	private function includes(){
		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-admin.php';
		$this->admin = WC_customer_email_verification_admin::get_instance();

		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-email.php';
		$this->email = WC_customer_email_verification_email::get_instance();		
		
		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-email-common.php';
	}

	/*
	* include file on plugin load
	*/
	public function on_plugins_loaded() {		
		require_once $this->get_plugin_path() . '/includes/customizer/class-cev-customizer.php';								
	}	
}

/**
 * Returns an instance of zorem_woo_il_post.
 *
 * @since 1.0
 * @version 1.0
 *
 * @return zorem_woo_il_post
*/
function woo_customer_email_verification() {
	static $instance;

	if ( ! isset( $instance ) ) {		
		$instance = new zorem_woo_customer_email_verification();
	}

	return $instance;
}

/**
 * Register this class globally.
 *
 * Backward compatibility.
*/
$GLOBALS['woo_customer_email_verification'] = woo_customer_email_verification();
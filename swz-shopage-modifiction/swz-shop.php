<?php

/*
Plugin Name: Swz Shoppage Modification
*/

add_filter( 'woocommerce_locate_template', 'intercept_wc_template', 10, 3 );
/**
 * Filter the cart template path to use cart.php in this plugin instead of the one in WooCommerce.
 *
 * @param string $template      Default template file path.
 * @param string $template_name Template file slug.
 * @param string $template_path Template file name.
 *
 * @return string The new Template file path.
 */
function intercept_wc_template( $template, $template_name, $template_path ) {

  $template_directory = trailingslashit( plugin_dir_path( __FILE__ ) ) . 'woocommerce/';
  $path = $template_directory . $template_name;

  return file_exists( $path ) ? $path : $template;

}

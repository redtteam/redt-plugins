<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once ABSPATH.'wp-includes/pluggable.php';
/*
Plugin Name: Swz WC Modification
*/

if ( ! defined( 'SWZ_WC_MODIFICATION_DIR' ) ) {
	define( 'SWZ_WC_MODIFICATION_DIR', dirname( __FILE__ ) );
}
if(session_id() == ''){
	session_start();
}
function swz_wc_default_crop_data(){
	$defaultCrop = array(
		"rect1" => array("width" => 42, "height" => 59.4),
		"rect2" => array("width" => 59.4, "height" => 84),
		"rect3" => array("width" => 84, "height" => 118.8),
		"rect4" => array("width" => 106, "height" => 150),
		"rect5" => array("width" => 150, "height" => 212),
		"sq1" => array("width" => 40, "height" => 40),
		"sq2" => array("width" => 60, "height" => 60),
		"sq3" => array("width" => 80, "height" => 80),
		"sq4" => array("width" => 100, "height" => 100),
		"sq5" => array("width" => 150, "height" => 150)
	);
	return $defaultCrop;
}
function swz_woocommerce_search_form()
{	
	$colors = swz_get_categories_by_parent_slug('colors');
	$artists = swz_get_categories_by_parent_slug('artist');
	
	
	//$artists = get_terms( 'product_tag' );	
	include_once 'templates/search-form.php';	
}
function swz_get_categories_by_parent_slug($slug){	
	$category = get_term_by( 'slug', $slug, 'product_cat' );
	$child_categories = array();
	if($category){
		$args = array(	
		'child_of'                 => $category->term_id,
		'orderby'                  => 'name',
		'order'                    => 'ASC',
		'hide_empty'               => FALSE,
		'hierarchical'             => 1,
		'taxonomy'                 => 'product_cat',
		); 
		$child_categories = get_categories($args );
	}	
	return $child_categories;
}
function swz_wc_advanced_search_query($query) {
	// var_dump($query);
   // if($query->is_search()) {
        // category terms search.
		$searchItems = array();
		$taxQuery = array();
        if (isset($_GET['swz_category_color']) && !empty($_GET['swz_category_color'])) {
            $searchItems[] = array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => $_GET['swz_category_color'],
			);
        }
		if (isset($_GET['swz_category_artist']) && !empty($_GET['swz_category_artist'])) {
            $searchItems[] = array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => $_GET['swz_category_artist'],
			);
        }
		$filterByWidth = array();
		$minWidth = 0;
		$maxWidth = 0;
		if (isset($_GET['swz_product_min_width']) && !empty($_GET['swz_product_min_width']))
		{			
            $minWidth = $_GET['swz_product_min_width'];
        }
		if (isset($_GET['swz_product_max_width']) && !empty($_GET['swz_product_max_width']))
		{			
            $maxWidth = $_GET['swz_product_max_width'];
        }
		if($minWidth){
			$searchItems[] = array(
				'taxonomy' => 'product_cat',
				'field' => 'name',
				'terms' => $minWidth,
			);
		}
		if($maxWidth){
			$searchItems[] = array(
				'taxonomy' => 'product_cat',
				'field' => 'name',
				'terms' => $maxWidth,
			);
		}
		/* if(!empty($maxWidth)){
			 $filterByWidth = range($minWidth,$maxWidth);
		}else{
			if(!empty($minWidth)){
			 $category = get_term_by( 'slug', 'product-width', 'product_cat' );	
			 $args = array('taxonomy' => 'product_cat','child_of' => $category->term_id, 'orderby' => 'slug', 'number' => 1, 'hide_empty' => FALSE, 'order' => 'DESC'); 	
			 $term_query = new WP_Term_Query( $args );  
			if($term_query->terms){	
				$maxWidth = $term_query->terms[0]->slug;
			}
			 
			$filterByWidth = range($minWidth,$maxWidth);
			}
		} */
		
		//$filterByWidth = array(100,110);
		if($filterByWidth){
			 $searchItems[] = array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => $filterByWidth,
			); 
		}
		
		// var_dump($searchItems);
		if($searchItems){
			$taxQuery = array('relation' => 'OR',$searchItems);
			$query->set('tax_query', $taxQuery);	
		}
		
   // }
	// var_dump($query); exit;
    return $query;
}
function swz_product_single_add_to_cart(){
	 return __( 'PURCHASE ORIGINAL', 'swz-add-to-cart-text' );
}
function swz_wc_customized_button(){
	$productID = get_the_ID();
	//echo '<a style="margin-left:5px;" class="single_add_to_cart_button button alt" href="'.site_url().'/customize-product/?p_id='.$productID.'">CUSTOMIZE ARTWORK</a>';
}

function swz_customize_product_shortcode_func(){
	if(isset($_REQUEST['p_id'])){
		ob_start();
		include_once 'templates/customized-product.php';
		return ob_get_clean();
	}
}
function swz_medium_frame_product_shortcode_func(){
	if(isset($_REQUEST['p_id'])){
		ob_start();
		include_once 'templates/medium-frame.php';
		return ob_get_clean();
	}
}
function swz_save_customized_settings(){
	$postdata = $_REQUEST;
	$productId = $postdata['product_id'];
    //$file_path = str_replace( $uploads['baseurl'], $uploads['basedir'], $url );
	 
	 
	 $imageUrl = swzWcGetPrinterImageUrl($productId);
	 if(empty($imageUrl)){
		 $attachment_ids[0] = get_post_thumbnail_id( $productId );
		 $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
	     $ext = pathinfo($attachment[0], PATHINFO_EXTENSION);
		 $fileUrl = $attachment[0];
	 }else{
		 $ext = pathinfo($imageUrl, PATHINFO_EXTENSION);
		 $fileUrl = $imageUrl;
	 }	 
	 
	 try{
	 $image = swz_customize_filtered_image($postdata,$fileUrl,$ext);
		if($image){
			echo json_encode(array("image" => $image,"status" => 1));	
		}else{
			echo json_encode(array("image" => $attachment[0],"status" => 0));
		}
	 }catch(Exception $e){
		echo json_encode(array("image" => "","status" => 0));
	 }
	exit;
}
function swz_tmp_folder_dir_remove($dir){
	$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
	$files = new RecursiveIteratorIterator($it,
				 RecursiveIteratorIterator::CHILD_FIRST);
	foreach($files as $file) {
		if ($file->isDir()){
			rmdir($file->getRealPath());
		} else {
			unlink($file->getRealPath());
		}
	}
	rmdir($dir);
}
function swz_customize_filtered_image($postdata,$image,$ext){
	
	$productId = $postdata['product_id'];
	$zoomLevel = $postdata['zoomlevel'];
	$brightness = empty($postdata['brightness']) ? 0 : $postdata['brightness'];
	$contrast = empty($postdata['contrast']) ? 0 : $postdata['contrast'];
	$imageWidth = empty($postdata['width_image']) ? "" : intval($postdata['width_image']) * 38;	
	$imageHeight = empty($postdata['height_image']) ? "" : intval($postdata['height_image']) * 38;
	$default_crop = $postdata['default_crop'];
	//unset($_SESSION['customizeProduct'][$productId]);	
	$file_dir = ABSPATH.'wp-content/uploads/swz-files';
	if(!isset($_SESSION['customizeProduct'][$productId])){
		
		$fileName = uniqid().'_'.$productId.'.'.$ext;
		$filePath = $file_dir.'/'.$fileName;
		copy($image,$filePath);
		$_SESSION['customizeProduct'][$productId]['original_image'] = $filePath;
		$_SESSION['customizeProduct'][$productId]['original_filename'] = $fileName;
		
	}else{
		$filePath = $_SESSION['customizeProduct'][$productId]['original_image'];
			
		if(!file_exists($filePath)){			
			copy($image,$filePath);
			$fileName = basename($filePath);	
			$_SESSION['customizeProduct'][$productId]['original_image'] = $filePath;
			$_SESSION['customizeProduct'][$productId]['original_filename'] = $fileName;
		}
	}
	
		
	$_SESSION['customizeProduct'][$productId]['postdata'] = $postdata;
	
	if($filePath){		
		$image = $_SESSION['customizeProduct'][$productId]['original_image_url'];
		$original_filename = $_SESSION['customizeProduct'][$productId]['original_filename'];
		
		$modified_filename = 'pre_image_modified_'.$original_filename;
		$modified_filename_post = 'post_image_modified_'.$original_filename;
		if($original_filename){
			$tmpFolder = pathinfo($original_filename,PATHINFO_FILENAME);
			$temp_file_dir = $file_dir.'/'.$tmpFolder;	
			if(is_dir($temp_file_dir)){
				swz_tmp_folder_dir_remove($file_dir.'/'.$tmpFolder);
			}
			mkdir($temp_file_dir,0777);	
			if(isset($postdata['default_crop_preset']) && $postdata['default_crop_preset'] == "attribute"){					
					$default_crop = "attribute";
					$postdata['attr_width'] = $postdata['preset_attr_width'];
					$postdata['attr_height'] = $postdata['preset_attr_height'];
			}
			if($default_crop){
				/* $defaultCrop = array("A4" => array('width' => 2480, 'height' => 3508), "A3" => array('width' => 3508, 'height' => 4961),"A2" => array('width' => 4961, 'height' => 7016),"A1" => array('width' => 7016, 'height' => 9933));	 */
				
				
				$defaultCrop = swz_wc_default_crop_data();
				$width = $defaultCrop[$default_crop]['width'];
				$height = $defaultCrop[$default_crop]['height'];
				
				
				if($default_crop == 'attribute'){
					$width = $postdata['attr_width'];
					$height = $postdata['attr_height'];
				}				
				$_SESSION['customizeProduct'][$productId]["crop_size"] = array("width" => $width, "height" => $height);
				
				$width = $width * 38; 
				$height = $height * 38; 
				$zoomlevelPer = '';
				if($zoomLevel){
					$zoomlevelPer = $zoomLevel.'%';
				}
				$convertCmd = "convert {$file_dir}/$original_filename -resize {$zoomlevelPer} {$width}x -brightness-contrast {$brightness}x{$contrast} -crop {$width}x{$height} {$temp_file_dir}/$modified_filename";
				//echo $convertCmd;
				exec($convertCmd,$output,$return);
				
			}else{
				if(!empty($postdata['crop_w'])){
						$zoomlevelPer = '';
						if($zoomLevel){
							$zoomlevelPer = '-resize '.$zoomLevel.'%';
						}
					$convertCmd = "convert {$file_dir}/$original_filename {$zoomlevelPer} -crop {$postdata['crop_w']}x{$postdata['crop_h']}+{$postdata['x1']}+{$postdata['y1']} -brightness-contrast {$brightness}x{$contrast}  {$temp_file_dir}/$modified_filename";	
					exec($convertCmd);
				}else{
					if(!empty($width) || !empty($height)){
						$zoomlevelPer = '';
						if($zoomLevel){
							$zoomlevelPer = $zoomLevel.'%';
						}
						if(!empty($width) && !empty($height)){
							$convertCmd = "convert {$file_dir}/$original_filename -resize {$zoomlevelPer} {$width}x{$height}\! -brightness-contrast {$brightness}x{$contrast} {$temp_file_dir}/$modified_filename";
						}else{
							$convertCmd = "convert {$file_dir}/$original_filename -resize {$zoomlevelPer} {$width}x{$height} -brightness-contrast {$brightness}x{$contrast} {$temp_file_dir}/$modified_filename";
						}
						//echo $convertCmd;exit;						
						exec($convertCmd);
					}else{
						if(!empty($brightness) || !empty($contrast)){
							$zoomlevelPer = '';
							if($zoomLevel){
								$zoomlevelPer = '-resize '.$zoomLevel.'%';
							}
							$convertCmd = "convert {$file_dir}/$original_filename {$zoomlevelPer}  -brightness-contrast {$brightness}x{$contrast} {$temp_file_dir}/$modified_filename";	
							exec($convertCmd);
						}else{
							if($zoomLevel){
								$zoomlevelPer = $zoomLevel.'%';
								$convertCmd = "convert {$file_dir}/$original_filename  -resize {$zoomlevelPer} {$temp_file_dir}/$modified_filename";	
								exec($convertCmd);
							}
						}
					}
				}
			}
			
			if(file_exists($temp_file_dir.'/'.$modified_filename)){
				//$image = site_url().'/wp-content/uploads/swz-files/'.$tmpFolder.'/'.$modified_filename;
				$printerImagePath = $temp_file_dir.'/'.$modified_filename;
				$orderFilePath = $temp_file_dir.'/'.$modified_filename_post;
				$convertCmd = "convert {$printerImagePath} -resize {$imageWidth}x{$imageHeight} {$orderFilePath}"; 			
				exec($convertCmd);
			}else{
				$modified_filename = pathinfo($modified_filename,PATHINFO_FILENAME);
				$modified_filename = $modified_filename.'-0.'.$ext;
				if(file_exists($temp_file_dir.'/'.$modified_filename)){
					//$image = site_url().'/wp-content/uploads/swz-files/'.$tmpFolder.'/'.$modified_filename;
					$printerImagePath = $temp_file_dir.'/'.$modified_filename;
					$orderFilePath = $temp_file_dir.'/'.$modified_filename_post;
					$convertCmd = "convert {$printerImagePath} -resize {$imageWidth}x{$imageHeight} {$orderFilePath}"; 
					exec($convertCmd);	
				}
			}
			if(file_exists($temp_file_dir.'/'.$modified_filename_post)){
				$image = site_url().'/wp-content/uploads/swz-files/'.$tmpFolder.'/'.$modified_filename_post;
			}else{
				$modified_filename_post = pathinfo($modified_filename_post,PATHINFO_FILENAME);
				$modified_filename_post = $modified_filename_post.'-0.'.$ext;
				$image = site_url().'/wp-content/uploads/swz-files/'.$tmpFolder.'/'.$modified_filename_post;
			}
			$_SESSION['customizeProduct']['temp_file_dir'] = $temp_file_dir;
			$_SESSION['customizeProduct'][$productId]['zoomlevel'] = $zoomlevel;
			if($image){
				$_SESSION['customizeProduct'][$productId]["modified_image"] = $image;
			}else{
				$_SESSION['customizeProduct'][$productId]["modified_image"] = $_SESSION['customizeProduct'][$productId]['original_image_url'];
			}
			swzWcSaveProductToWishlist($_SESSION['customizeProduct'][$productId],$productId);
		}
		
	}
	return $image;
}
function swz_saved_customized_add_to_cart(){
	global $woocommerce;
	$requestdata = $_POST;
	// var_dump($requestdata); exit;
	$productId = $requestdata['product_id'];
	$postdata = $_SESSION['customizeProduct'][$productId];
	// $postdata['frame'] = $requestdata['frame'];
	$postdata['medium'] = $requestdata['medium'];
	$postdata['quantity'] = intval($requestdata['quantity']);
	$_SESSION['customizeProduct'][$productId] = $postdata;

	echo $woocommerce->cart->add_to_cart($productId, $postdata['quantity']);	
	exit;
}
function swz_remove_customized_product_from_cart(){
	$requestdata = $_REQUEST;
	$productId = $requestdata['product_id'];
	if(isset($_SESSION['customizeProduct'][$productId])){
		unset($_SESSION['customizeProduct'][$productId]);
	}
}
function swz_wc_custom_email_notification($order_id){

	if ( ! $order_id ){
		return;	
	}
	$order = wc_get_order( $order_id );
	$productIds = array();
	$productdetails = array();
	foreach ( $order->get_items() as $item_id => $order_item ) {

        // Accessing to the protected data of the WC_Order_Item_Product object
        $order_item_data = $order_item->get_data();

        // Get the associated WC_Product object
        $product = $order_item->get_product();

        // Accessing to the WC_Product object protected data
        $product_data = $product->get_data();
		$productIds[] = $product_data['id'];
		$productdetails[$product_data['id']] = $product_data;
    }
	
	$filteredProducts = array();
	
	$savedData = get_post_meta( $order_id, '_order_swz_wc_custom_details', true );
	
	$sampleFolder = ABSPATH.'wp-content/uploads/order-image-samples/';
	if(empty($savedData)){
		if(!is_dir($sampleFolder)){
			mkdir($sampleFolder,0777);
		}
		$orderFolder = $sampleFolder.$order_id;
		if(!is_dir($orderFolder)){
			mkdir($orderFolder,0777);
		}
		if($productIds && isset($_SESSION['customizeProduct'])){
			$customizedProducts = $_SESSION['customizeProduct'];			
			if($customizedProducts){
				foreach($customizedProducts as $productId => $cmp){
					if(!is_numeric($productId)){
						continue;
					}
					if(!in_array($productId,$productIds)){
						continue;
					}
					
					$remodified = 0;
					if(empty($cmp["modified_image_url"])){
						$modified_image = swzWcGetPrinterImageUrl($productId);;
					}else{
						$modified_image = $cmp["modified_image_url"];
						$remodified = 1;
					}
					if($remodified){
						$modified_url = $cmp['modified_image_url'];
						$postdata = $cmp;
						$rootpath = rtrim(ABSPATH,'/');
						$siteUrl = site_url();
						$imageUrl = swzWcGetPrinterImageUrl($productId);
						$imageUrl = str_replace("http:","https:",$imageUrl);			
						$printerImagePath = str_replace($siteUrl,$rootpath,$imageUrl);
						$ext = pathinfo($printerImagePath, PATHINFO_EXTENSION);
						
						$file_dir = ABSPATH.'wp-content/uploads/swz-files-doka';
						$tmpFolder = "order-temp_".$order_id;
						$temp_file_dir = $file_dir.'/'.$tmpFolder;
						$modified_filename = 'print_ready_file_'.$productId.'.'.$ext;
						if(!is_dir($temp_file_dir)){
							mkdir($temp_file_dir,0777);
						}	
						
						$zoomlevelPer = $zoomlevelPer = $postdata['zoom'].'%';
						$convertCmd = "convert {$printerImagePath} {$zoomlevelPer}  -gravity Center -crop {$postdata['crop_width']}x{$postdata['crop_height']}+{$postdata['crop_x']}+{$postdata['crop_y']} {$temp_file_dir}/$modified_filename";
						
						exec($convertCmd);
						
						$imageWidth = $postdata['width_image'] * 38;
						$imageHeight = $postdata['height_image'] * 38;
						$orderFilePath = ABSPATH.'wp-content/uploads/order-image-samples/'.$order_id.'/'.$order_id.'_'.$productId.'.'.$ext;
						if(file_exists($temp_file_dir.'/'.$modified_filename)){
							$printerImagePath = $temp_file_dir.'/'.$modified_filename;
						}else{
							$modified_filename = pathinfo($modified_filename,PATHINFO_FILENAME);
							$modified_filename = $modified_filename.'-0.'.$ext;
							if(file_exists($temp_file_dir.'/'.$modified_filename)){
								$printerImagePath = $temp_file_dir.'/'.$modified_filename;
							}							
						}
						$convertCmd = "convert {$printerImagePath} -resize {$imageWidth}x{$imageHeight} -density 300 {$orderFilePath}"; 
							
						exec($convertCmd);
						
						
						$modified_image = site_url().'/wp-content/uploads/order-image-samples/'.$order_id.'/'.$order_id.'_'.$productId.'.'.$ext;
						
					}else{
						if($modified_image){
							$ext = pathinfo($modified_image, PATHINFO_EXTENSION);
							copy($modified_image,$orderFolder.'/'.$productId.'.'.$ext);
							$modified_image = site_url().'/wp-content/uploads/order-image-samples/'.$order_id.'/'.$order_id.'_'.$productId.'.'.$ext;
						}
					}
					if($modified_url){
						copy($modified_url,ABSPATH.'wp-content/uploads/order-image-samples/'.$order_id.'/mockup_'.$order_id.'_'.$productId.'.'.$ext);
						if(file_exists($cmp["modified_image_path"])){
							unlink($cmp["modified_image_path"]);
						}
					}
					
					$mockupImage = site_url().'/wp-content/uploads/order-image-samples/'.$order_id.'/mockup_'.$order_id.'_'.$productId.'.'.$ext;
					$filteredProducts[$productId] = array("id" => $productId, "name" => $productdetails[$productId]["name"],"original_image" => $originalImageUrl, "sku" => $productdetails[$productId]["sku"], "description" => $productdetails[$productId]["name"],"modified_image" => $modified_image,'mockup_image' => $mockupImage ,"postdata" => $cmp); 
				}
				
				$savedData = serialize($filteredProducts);				
				if($temp_file_dir){
					if(is_dir($temp_file_dir)){
						swz_tmp_folder_dir_remove($temp_file_dir);
					}
				}
				add_post_meta($order_id,'_order_swz_wc_custom_details', $savedData);
				unset($_SESSION['customizeProduct']);
				sendSwzCustomEmailToPrinter($savedData,$order_id);
				
			}
			
		}
	}
	
}
function sendSwzCustomEmailToPrinter($savedData,$order_id){
	$to = get_option( 'woocommerce_printer_email', 1 );
	$savedData = unserialize($savedData);

	//$to = 'ravnish.smartwinz@gmail.com';
	$headers = array('Content-Type: text/html; charset=UTF-8');
    $subject = "New Order Created #".$order_id;
    $content = "<table>";
    $content .= "<tr>";
    $content .= "<th>#</th>";
    $content .= "<th>Product Name</th>";
    $content .= "<th>Sku</th>";    
    $content .= "<th>Original Image</th>";
    $content .= "<th>Mockup Image</th>";
    $content .= "<th>Printer Image</th>";
    $content .= "</tr>";
	
	$counter = 1;
	foreach($savedData as $item){
		$attachment_ids[0] = get_post_thumbnail_id( $item['id'] );
		$attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
		$content .= '<tr>';
		$content .= '<td>'.$counter++.'</td>';
		 $content .= "<td><a href='".get_permalink($item['id'])."'>".$item['name']."</a></td>";
		 $content .= "<td>".$item['sku']."</th>";
		 $content .= "<td><a href='".$attachment[0]."'><img style='width:120px;height:120px;' src='".$attachment[0]."'/></a></td>";
		 $content .= "<td><a href='".$item['mockup_image']."'><img style='width:120px;height:120px;' src='".$item['mockup_image']."'/></a></td>";
		 $content .= "<td><a href='".$item['modified_image']."'>Printer Image</a></td>";
		
		 $content .= '</tr>';		
		 $content .= '<tr>';		
		 $content .= '<td colspan="5" style="border-bottom:2px solid #000;">';
		 
		 $content .= '<table>';	
		 $content .= '<tr>';
		 $content .= '<th>Crop</th>';	
		 $content .= '<th>Contrast</th>';	
		 $content .= '<th>Brightness</th>';	
		 $content .= '<th>Medium</th>';	
		 $content .= '<th>Frame</th>';	
		 $content .= '<th>Quantity</th>';
		 $content .= '</tr>';	
		 $content .= '<tr>';	
		 
		$content .= '<td>'.$item['postdata']['width_image'].'X'.$item['postdata']['height_image'].' cm</td>';
		 $content .= '<td></td>';	 
		 $content .= '<td></td>';	 
		 
		 /* $content .= '<td>'.$item['contrast'].'</td>';			 
		 $content .= '<td>'.$item['brightness'].'</td>';  */		 
		 $content .= '<td>'.$item['postdata']['medium'].'</td>'; 	 
		 $content .= '<td>'.$item['postdata']['frame'].'</td>';			 
		 $content .= '<td>'.$item['postdata']['quantity'].'</td>'; 
		 $content .= '</tr>'; 
		 $content .= '</table>';	
		 $content .= '</td>';
		 $content .= '</tr>';
	}
    $content .= "</table>";
	$adminEmail = get_bloginfo('admin_email');
	//$adminEmail = 'admin.smartwinz@yopmail.com';
	wp_mail( $to, $subject, $content, $headers );
	wp_mail( $adminEmail, $subject, $content, $headers );
}
function swz_wc_filter_woocommerce_general_settings($settings){
	foreach ( $settings as $section ) {
	 if ( isset( $section['id'] ) && 'general_options' == $section['id'] &&
       isset( $section['type'] ) && 'sectionend' == $section['type'] ) {	
	 $updated_settings[] = array(
					'name'     => __( 'Printer Email', 'woocommerce' ),
					'desc'     => __( 'This sets default printer email after each checkout.', 'woocommerce' ),
					'id'       => 'woocommerce_printer_email',
					'css'      => 'width:200px;',
					'std'      => 'johndoe@yopmail.com', 
					'default'  => 'johndoe@yopmail.com', 
					'type'     => 'text',
					'desc_tip' =>  true,
				  );
	}
	$updated_settings[] = $section;
	}	
	return $updated_settings;
}
function swz_wc_add_custom_price($cart_object){
	$perCentage = 185;	
	
	foreach ( $cart_object->cart_contents as $value ) {
		
		$productId = $value['product_id'];		
		if(isset($_SESSION['customizeProduct'][$productId])){
			 $product = wc_get_product( $productId );
			 $productPrice = $product->get_price();
			if(isset($_SESSION['customizeProduct'][$productId]['crop_size'])){
				$cropSize = $_SESSION['customizeProduct'][$productId]['crop_size'];
			}else{
				$cropSize = array();
				$cropSize['width'] = $_SESSION['customizeProduct'][$productId]['width_image'];
				$cropSize['height'] = $_SESSION['customizeProduct'][$productId]['height_image'];
			}			
			
			if(!empty($cropSize['width']) && !empty($cropSize['height'])){
				$cropW = $cropSize['width'];
				$cropH = $cropSize['height'];
				if($cropW > 100 && $cropH > 100){
					$perCentage = 110;
				}
				if($cropW <= 100 && $cropH <= 100){
					if($cropW > 84 || $cropH > 60){
						$perCentage = 120;
					}
				}
				if($cropW <= 84 && $cropH <= 60){
					if($cropW > 42 || $cropH <= 60){
						$perCentage = 145;
					}
				}
				if($cropW <= 42 && $cropH <= 60){
					if($cropW <= 42 || $cropH >= 30){
						$perCentage = 160;
					}
				}
				if($cropW <= 42 && $cropH <= 30){
					if($cropW >= 21 || $cropH >= 29.7){
						$perCentage = 185;
					}
				}
				if($cropW <= 21 && $cropH <= 29.7){
					$perCentage = 185;
				}
				if($cropW <= 120){
					$framePrice = 5480;
				}
				if($cropW < 106){
					$framePrice = 4270;
				}
				if($cropW < 84){
					$framePrice = 3020;
				}
				if($cropW < 59.4){
					$framePrice = 1880;
				}
				if($cropW <= 42){
					$framePrice = 1330;
				}
				if($cropW > 120){
					$framePrice = 5480;
				}
								
				$actualPrice = $productPrice;
				
				$productPrice = $actualPrice + ((($cropW * $cropH) * 0.135 *($perCentage/100)) * ($perCentage/100));
				
				
				if(isset($_SESSION['customizeProduct'][$productId]['postdata']['frame'])){
					$isValidFrame = strtolower($_SESSION['customizeProduct'][$productId]['postdata']['frame']);
					if($isValidFrame != 'no'){
						$productPrice = $productPrice + $framePrice; 
					}
				}
				$productPrice = number_format((float)$productPrice, 2, '.', '');
				$value['data']->set_price($productPrice);	
			}
			
		}
	}
}
/* function swz_wc_woocommerce_before_cart(){
	
} */
function swz_wc_custom_menu_items_event($menu_links){
	$new = array( 'collections' => 'My Collection' );
	$menu_links = array_slice( $menu_links, 0, 1, true ) + $new + array_slice( $menu_links, 1, NULL, true );
	return $menu_links;
}

add_action( 'init', 'my_account_swz_new_endpoints' );

function my_account_swz_new_endpoints() {
add_rewrite_endpoint( 'collections', EP_ROOT | EP_PAGES );
}
add_action( 'woocommerce_account_collections_endpoint', 'collections_endpoint_content_swz_wc' );
function collections_endpoint_content_swz_wc() {
 echo do_shortcode('[yith_wcwl_wishlist]');
}
/* add_action( 'woocommerce_before_cart', 'swz_wc_woocommerce_before_cart'); */
// add_action( 'woocommerce_thankyou', 'swz_wc_custom_email_notification', 10, 1 );
add_action ( 'woocommerce_archive_description' ,  'swz_woocommerce_search_form');

// add_action('pre_get_posts', 'swz_wc_advanced_search_query', 1000);

add_filter( 'woocommerce_product_single_add_to_cart_text', 'swz_product_single_add_to_cart' );

add_filter( 'woocommerce_general_settings', 'swz_wc_filter_woocommerce_general_settings', 10, 1 ); 

add_action('woocommerce_after_add_to_cart_button','swz_wc_customized_button');

add_shortcode( 'swz_customize_product_shortcode', 'swz_customize_product_shortcode_func' );

add_shortcode( 'swz_medium_frame_product_shortcode', 'swz_medium_frame_product_shortcode_func' );

add_action( 'wp_ajax_nopriv_swz_save_customized_settings', 'swz_save_customized_settings' );
add_action( 'wp_ajax_nopriv_swz_save_customized_updated_settings', 'swz_save_customized_updated_settings' );
add_action( 'wp_ajax_nopriv_swz_saved_customized_add_to_cart', 'swz_saved_customized_add_to_cart' );
add_action( 'wp_ajax_nopriv_swz_remove_customized_product_from_cart', 'swz_remove_customized_product_from_cart' );

/*Logged in user ajax*/
add_action( 'wp_ajax_swz_save_customized_settings', 'swz_save_customized_settings' );
add_action( 'wp_ajax_swz_save_customized_updated_settings', 'swz_save_customized_updated_settings' );
add_action( 'wp_ajax_swz_saved_customized_add_to_cart', 'swz_saved_customized_add_to_cart' );
add_action( 'wp_ajax_swz_remove_customized_product_from_cart', 'swz_remove_customized_product_from_cart' );

add_action( 'woocommerce_before_calculate_totals', 'swz_wc_add_custom_price' );
add_filter( 'woocommerce_account_menu_items', 'swz_wc_custom_menu_items_event', 99, 1 );

 /*if(isset($_REQUEST['swz_test_code_image_new'])){	
	$file_dir = ABSPATH.'wp-content/uploads/swz-files';
	$productId = 461;
	$ext = 'jpg';
	$image = "https://cdn.redtmultiples.com/wp-content/uploads/2019/11/05040612/AS001_B_A2.jpg";
	$filePath = $file_dir.'/'.uniqid().'_'.$productId.'.'.$ext;
	copy($image,$filePath);
	/* exec("convert {$file_dir}/comp23.png {$file_dir}/comp-2.jpg",$output, $return);
	print_r($output);
	print_r($return); */		
	
	//$file_dir = ABSPATH.'wp-content/uploads/swz-files';
	/* if(is_dir($file_dir)){
		//mkdir($file_dir,0777);
		copy("https://weddingmydeals.com/vendors_assets/images/cats/thumbnail/wedding-photographers.png",$file_dir."/comp23.png");
	} 
	exit('1312'); 
}*/
if(isset($_REQUEST['swz_save_customized_settings_call'])){
	swz_save_customized_settings();
	exit;
}

add_filter('woocommerce_default_catalog_orderby', 'swz_wc_custom_default_catalog_orderby');

function swz_wc_custom_default_catalog_orderby() {
     return 'rand'; 
}
if ( ! function_exists( 'swz_wc_print_attribute_radio' ) ) {
	function swz_wc_print_attribute_radio( $checked_value, $value, $label, $name ) {
		global $product;

		$input_name = 'attribute_' . esc_attr( $name ) ;
		$esc_value = esc_attr( $value );
		$id = esc_attr( $name . '_v_' . $value . $product->get_id() ); //added product ID at the end of the name to target single products
		$checked = checked( $checked_value, $value, false );
		$filtered_label = apply_filters( 'woocommerce_variation_option_name', $label, esc_attr( $name ) );
		printf( '<div><input type="radio" name="%1$s" value="%2$s" id="%3$s" %4$s><label for="%3$s">%5$s</label></div>', $input_name, $esc_value, $id, $checked, $filtered_label );
	}
}

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'swz_wc_loop_shop_per_page', 20 );

function swz_wc_loop_shop_per_page( $cols ) {
  $cols = 10;
  return $cols;
}
add_action( 'woocommerce_product_query', 'swz_wc_woocommerce_product_query' );
function swz_wc_woocommerce_product_query( $q ) {
	if ( $q->is_main_query() && ( $q->get( 'wc_query' ) === 'product_query' ) ) {
	$q->set( 'posts_per_page', '40' );
	}
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 80 );


// Add custom Meta box to admin products pages
add_action( 'add_meta_boxes', 'create_product_the_artist_detail_meta_box' );
function create_product_the_artist_detail_meta_box() {
    add_meta_box(
        'the_artist_product_meta_box',
        __( 'The Artist', 'cmb' ),
        'add_the_artist_content_meta_box',
        'product',
        'normal',
        'default'
    );
	add_meta_box(
        'the_more_info_product_meta_box',
        __( 'More Information', 'cmb' ),
        'add_the_more_info_product_content_meta_box',
        'product',
        'normal',
        'default'
    );
}

// Custom metabox content in admin product pages
function add_the_more_info_product_content_meta_box( $post ){
    $product = wc_get_product($post->ID);
    $content = $product->get_meta( '_the_more_info_product' );

    echo '<div class="the_more_info_product_meta_box">';

    wp_editor( $content, '_the_more_info_product', ['textarea_rows' => 10]);

    echo '</div>';
}

// Custom metabox content in admin product pages
function add_the_artist_content_meta_box( $post ){
    $product = wc_get_product($post->ID);
    $content = $product->get_meta( '_the_artist_detail' );

    echo '<div class="the_artist_product_meta_box">';

    wp_editor( $content, '_the_artist_detail', ['textarea_rows' => 10]);

    echo '</div>';
}

// Save WYSIWYG field value from product admin pages
add_action( 'woocommerce_admin_process_product_object', 'save_product_the_artist_detail_wysiwyg_field', 10, 1 );
function save_product_the_artist_detail_wysiwyg_field( $product ) {
    if (  isset( $_POST['_the_artist_detail'] ) ){
         $product->update_meta_data( '_the_artist_detail', wp_kses_post( $_POST['_the_artist_detail'] ) );
	}
	if (  isset( $_POST['_the_more_info_product'] ) ){
         $product->update_meta_data( '_the_more_info_product', wp_kses_post( $_POST['_the_more_info_product'] ) );
	}
}

// Add "technical specs" product tab
add_filter( 'woocommerce_product_tabs', 'add_the_artist_detail_product_tab', 10, 1 );
function add_the_artist_detail_product_tab( $tabs ) {
    $tabs['_the_artist_detail'] = array(
        'title'         => __( 'The Artist', 'woocommerce' ),
        'priority'      => 9,
        'callback'      => 'display_the_artist_detail_product_tab_content'

    );
	
	$tabs['_the_more_info_product'] = array(
        'title'         => __( 'More Information', 'woocommerce' ),
        'priority'      => 11,
        'callback'      => 'display_the_more_info_product_tab_content'

    );	

    return $tabs;
}

// Display "technical specs" content tab
function display_the_artist_detail_product_tab_content() {
    global $product;
    echo '<div class="wrapper-the_artist_detail" style="text-align:justify;">' . apply_filters('the_content',$product->get_meta( '_the_artist_detail' )) . '</div>';
}
function display_the_more_info_product_tab_content() {
    global $product;
    echo '<div class="wrapper-the_more_info_product" style="text-align:justify;">' . apply_filters('the_content',$product->get_meta( '_the_more_info_product' )) . '</div>';
}
 function swzWcSaveProductToWishlist($savedata,$productId){
	global $current_user;
	$current_user = wp_get_current_user();
	$userId = $current_user->ID;
	$savedata = json_encode($savedata);
	if($userId){
	   global $wpdb;
		$tableName = $wpdb->prefix . 'yith_wcwl';
		$sql = "SELECT * FROM ".$tableName." WHERE prod_id=".$productId." and user_id=".$userId;
		
		$result = $wpdb->get_row($sql);
		
		if($result){
			$sql = "UPDATE ".$tableName." SET crop_image_data='$savedata' WHERE id=".$result->id;
		
		} 
	}
}
/*
if(isset($_REQUEST['check_wishlist_add_custom_code'])){
	global $wpdb;
	$tableName = $wpdb->prefix . 'yith_wcwl';
	$sql = "SELECT * FROM ".$tableName;
	$results = $wpdb->get_results($sql);
	echo "<pre>";
	print_R($results);
	exit;
	
} */
function swzWcGetPrinterImageUrl($productId){
	$postId = get_post_meta( $productId, 'printer_img', true );
	$data = get_post($postId);
	$guid = $data->guid;
	return $guid;
}

if(isset($_REQUEST['restore_wishlist_product']) && $_REQUEST['swz_prod_id']){
	$prod_id = $_REQUEST['swz_prod_id'];
	if($prod_id){
		global $wpdb;
		$tableName = $wpdb->prefix . 'yith_wcwl';
		$sql = "SELECT * FROM ".$tableName." WHERE prod_id=".$prod_id;
		$result = $wpdb->get_row($sql);
		
		if($result){
			$crop_image_data = $result->crop_image_data;
			
			if($crop_image_data){
				$_SESSION['customizeProduct'][$prod_id] = json_decode($crop_image_data,true);
				$safeUrl = site_url('/medium-frame/?p_id='.$prod_id);
				
				wp_safe_redirect($safeUrl);
				exit;
			}
		}
		
	}
	$safeUrl = site_url();
	wp_safe_redirect($safeUrl);
	exit;	
	
}
function swz_save_customized_updated_settings(){
	
	$postdata = $_REQUEST;
	$cropped_image = $_FILES['cropped_image'];	
	
	$productId = $postdata['product_id'];
	$file_dir = ABSPATH.'wp-content/uploads/swz-files-doka';
	if(!is_dir($file_dir)){
		mkdir($file_dir,0777);	
	}
	$newItem = array();
	$newItem["width_image"] = $postdata["width_image"];
	$newItem["height_image"] = $postdata["height_image"];
	$newItem["crop_x"] = $postdata["crop_x"];
	$newItem["crop_y"] = $postdata["crop_y"];
	$newItem['crop_width'] = $postdata['crop_width'];
	$newItem['crop_height'] = $postdata['crop_height'];
	$newItem["zoom"] = $postdata["zoom"];
	$newItem["product_id"] = $postdata["product_id"];
	$imageUrl = $postdata['imageurl'];
	$newItem["folder"] = $file_dir;
	$ext = "jpg";

	
	if(!isset($_SESSION['customizeProduct'][$productId])){		
		$fileName = uniqid().'_'.$productId.'.'.$ext;
		$filePath = $file_dir.'/'.$fileName;		
		move_uploaded_file($cropped_image['tmp_name'],$filePath);		
		$newItem["modified_image_path"] = $filePath;
		$newItem["modified_image_url"] = site_url('/wp-content/uploads/swz-files-doka/'.$fileName);			
		$_SESSION['customizeProduct'][$productId] = $newItem;		
	}else{
		$filePath = $_SESSION['customizeProduct'][$productId]["modified_image_path"];
		$newItem["modified_image_path"] = $_SESSION['customizeProduct'][$productId]["modified_image_path"];
		$newItem["modified_image_url"] = $_SESSION['customizeProduct'][$productId]["modified_image_url"];
		$_SESSION['customizeProduct'][$productId] = $newItem;
		if(file_exists($filePath)){
			unlink($filePath);
		}
		move_uploaded_file($cropped_image['tmp_name'],$filePath);
		swzWcSaveProductToWishlist($_SESSION['customizeProduct'][$productId],$productId);
	}
	/* $imageUrl = swzWcGetPrinterImageUrl($productId);
	$rootpath = rtrim(ABSPATH,'/');
	$siteUrl = site_url();
	$imageUrl = str_replace("http:","https:",$imageUrl);			
	$printerImagePath = str_replace($siteUrl,$rootpath,$imageUrl);
	$ext = pathinfo($printerImagePath, PATHINFO_EXTENSION);
	
	$modified_filename = 'checking_image.'.$ext;
	$zoomlevelPer = $postdata['zoom'].'%';
	$cropX = $postdata['crop_x'] +  $postdata['crop_width'] /2;
	$cropY = $postdata['crop_y'] + $postdata['crop_height'] /2;
	$convertCmd = "convert {$printerImagePath} -resize {$postdata['crop_width']}x{$postdata['crop_height']} {$zoomlevelPer}  -gravity Center -crop {$postdata['crop_width']}x{$postdata['crop_height']}+{$cropX}+{$cropY} -brightness-contrast {$brightness}x{$contrast}  {$file_dir}/$modified_filename";	
	echo $convertCmd;
	exec($convertCmd); */
	echo json_encode($newItem); 
	exit;	
}

/* add_filter( 'woocommerce_currency_symbol', 'swz_wc_modify_custom_currency_symbol',10, 1 );
function swz_wc_modify_custom_currency_symbol( $currency ) {
   return 'HKD'.$currency;
} */
<style>
.width-label{
	margin-left:4%;
	margin-top:4%;
}
#swzWcSearchForm{
	margin-top:7%;
}
.select-wrapper:after {
    content: "\f107";
    font-family: FontAwesome;
    right: 13px;
    font-size: 24px;
    color: #6d6d6d;
	position:absolute;
	top:0;
	margin-top:10px;
	pointer-events:none;
}
.select-wrapper select{
	border:none;
}
/* .select-wrapper option,.select-wrapper select,.select-wrapper select option:focus{
	color:#FFF;
	background: #333333!important;
} */
.pos-relative{
	position:relative;
}
.input-size-field,.input-size-field:focus{
	border: none !important;;
    background: #FFF !important;
    color: #6d6d6d !important;
    border-bottom: 1px solid #6d6d6d !important;
}
/* @media only screen and (min-width: 1240px){
.section_wrapper, .container {
    max-width: 1220px;
}
} */
.shop-filters{
	padding:0;
}
#Content .section_wrapper{
	max-width:100%;
	width: 98%;
	float:left;
	margin:0 1% 40px;
}
#swzWcSearchForm .column{
	margin-bottom:0;
}
.woocommerce-info.alert.alert_info{
	background:#bfbfbf;
}
.changeSelectOpt{
	-webkit-appearance:none;
}
.isotope_wrapper {
    padding-left: 24px;
}
</style>
<form id="swzWcSearchForm" style="min-height:80px;margin-left:0;" method="get" action="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">
	<div class="one column" style="margin-bottom:0;">
		<div class="one-sixth column pos-relative">			
			<div class="select-wrapper">
				<select name="swz_category_color" class="column changeSelectOpt">
					<option value="">All Colors</option>
					<?php foreach($colors as $color): ?>
					<option <?php echo (isset($_REQUEST['swz_category_color']) && $_REQUEST['swz_category_color'] == $color->slug) ? 'selected="selected"' : ''; ?> value="<?php echo $color->slug; ?>"><?php echo $color->name; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="one-sixth column pos-relative">
			<div class="select-wrapper">
				<select name="swz_category_artist" class="changeSelectOpt">
					<option value="">All Artists</option>
					<?php foreach($artists as $artist): ?>
					<option <?php echo (isset($_REQUEST['swz_category_artist']) && $_REQUEST['swz_category_artist'] == $artist->slug) ? 'selected="selected"' : ''; ?> value="<?php echo $artist->slug; ?>"><?php echo $artist->name; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<!--div class="one-fourth column">
			<label class="width-label one-third  column">Min Width</label>
			<input type="text" style="background:#FFF!important;" class="changeSelectOpt one-third column input-size-field" placeholder="" value="<?php echo (isset($_REQUEST['swz_product_min_width'])) ? $_REQUEST['swz_product_min_width'] : ''; ?>" name="swz_product_min_width" />
			<label class="width-label one-fifth  column" style="color: #6d6d6d">CM</label>
		</div>
		<div class="one-fourth column">	
			<label class="width-label one-third  column" style="color: #6d6d6d">Max Width</label>
			<input type="text" style="background:#FFF!important;" class="changeSelectOpt one-third column input-size-field" placeholder="" value="<?php echo (isset($_REQUEST['swz_product_max_width'])) ? $_REQUEST['swz_product_max_width'] : ''; ?>" name="swz_product_max_width" />
			<label class="width-label one-fifth column" style="color: #6d6d6d">CM</label>
		</div-->
	</div>	
</form>
<script>
jQuery(document).ready(function($){
	$(".changeSelectOpt").change(function(){
		$("#swzWcSearchForm").submit();
	});
});
</script>
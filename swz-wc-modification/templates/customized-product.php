<?php 
 $productId = $_REQUEST['p_id'];	
 $attachment_ids[0] = get_post_thumbnail_id( $productId );
 $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
 $product = wc_get_product( $productId );
 $productPrice = $product->get_price();
 
 
 $filename = basename($attachment[0]);
 /* echo "<pre>";
 print_r($filename);
 echo "</pre>"; */
 $doka_folder = ABSPATH.'wp-content/uploads/doka-product-images';
 if(!is_dir($doka_folder)){
	 mkdir($doka_folder,0777);	 
 }
 if(!file_exists($doka_folder.'/'.$filename)){
	 copy($attachment[0],$doka_folder.'/'.$filename);
 }


 $sizes = $product->get_variation_attributes();
 $attributes = array();
 if(isset($sizes['pa_select-size'])){
	$sizes = $sizes['pa_select-size'];
/* 	echo "<pre>";
	
	print_r($termdata);
	echo "</pre>"; */
	foreach($sizes as $size){
		
		$newAttr = str_replace('-cm','',$size);
		$newAttr = str_replace('a4','',$newAttr);
		$newAttr = explode('-x-',$newAttr);
		$saveAttr = array();
		if(isset($newAttr[0])){
			$termName = get_term_by('slug',$size,'pa_select-size');
			$saveAttr['width'] = str_replace('-','.',$newAttr[0]);
			$saveAttr['height'] = str_replace('-','.',$newAttr[1]);
			$saveAttr['name'] = $termName->name;
			$attributes[] = $saveAttr;
		}
		
	}
 }
?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href = "<?php echo plugins_url('/swz-wc-modification/templates/'); ?>css/doka.min.css" rel = "stylesheet">
<script src="<?php echo plugins_url('/swz-wc-modification/templates/'); ?>js/doka.jquery.min.js"></script> 
<div id="imageCropReal" style="display:none;">
	<img src="<?php echo $attachment[0]; ?>"/>
</div>
<style>
button.customized_homepage_button,a.customized_homepage_button,button.customized_homepage_button:hover,a.customized_homepage_button:hover{
    padding: 10px 30px;
    font-weight: bold;
    font-size: 11px;
    letter-spacing: 2px;
    background: #FFF!important;
   
    color: #000!important;
    border-radius: 19px;
    background-color: #FFF!important;
    /*border: none!important;*/
    border:2px solid #000;
    text-transform: uppercase;
	width:auto!important;
	text-deocration:none!important;
}
.cursor-pointer{
	cursor:pointer;
}
.black-color{
	color:#000;
}
#coords{
	display:none;
}
#processing-bar{
	font-size: 19px;   
    color: #f00;
    position: absolute;
    top: 20%;
    left: 50%;
    background: #ccc;
    padding: 7px;
    font-weight: bold;
	display:none;
}
#imageBlock.processing #processing-bar{
	display:block;
}
#imageBlock.processing img{
	opacity:0.5;
}
/* #contrast-slider
{
	width:90%;
	height:3px;
	margin-top:10px;
	background-color:#333333;
	border:1px solid #333333;
	margin-left:16px;
}
#brightness-slider
{
	width:90%;
	height:3px;
	margin-top:10px;
	margin-left:16px;
	background-color:#333333;
	border:1px solid #333333;
	
} */
#contrast-slider a.ui-slider-handle.ui-state-default.ui-corner-all,#brightness-slider a.ui-slider-handle.ui-state-default.ui-corner-all
{
	height:17px;
	width:17px;
	border-radius:25px;
	left:50%;
	border:1px solid #fff;
	background:#000;
	margin-top:-3px !important; 
}
.zeroElement {
    float: right;
    
}
.zeroElement2 {
    float: right;   
}
.customization-step-1,.customization-step-2{
	display:none;
}
.customization-step-2.active,.customization-step-1.active{
	display:block;
}
.heading1{	
    font-weight: bolder;
    font-size: 20px;
}
.heading5,.heading6{
	margin-bottom: 0;
}
.side-panel {    
    padding: 10px;
    color: #5a5a5a;  
	padding-top:0;	
}
.btn-custom-swz {
    background-color: #e5e5e5;
    margin-top: 17px;    
    /*color: #fff !important;*/
    text-align: center;
    padding: 14px;
    border-radius: 35px;
    font-weight: bold;
    font-size: 15px;
    letter-spacing: 2px;
}
.margin-bottom-10{
	margin-bottom: 10px;
}
.reset-option {
    border-bottom: 1px solid red;
    padding-bottom: 1px;
    letter-spacing: 1px;
    margin-top: 22px;
    font-size: 13px;
    text-transform: uppercase;
    text-decoration: none!important;
    color: #FFF;
}
 .jcrop-holder img, img.jcrop-preview { max-width: none !important } 
 .cropImage.img{
	padding: 10px;
    border-radius: 100%;
    float: left;
    border: 1px solid #000;
    padding-top: 6px;
 }
 .cropImage.img img{
	width: 20px;
    margin-top: 4px;
 }
 .progressbar-swz-wc {
  counter-reset: step;
}
.progressbar-swz-wc li {
  list-style: none;
  display: inline-block;
  width: 17.33%;
  position: relative;
  text-align: center;
  cursor: pointer;
  color: #5a5a5a;
  font-family:Lato;
  font-size:12px;
  font-weight:700;
}
.progressbar-swz-wc li:before {
  content: "";  
  width: 8px;
  height: 8px;
  line-height : 30px;
  border: none;
 
  border-radius: 100%;
  display: block;
  text-align: center;
  margin: 12px auto 5px auto;
  background-color: #5a5a5a;
}
.progressbar-swz-wc li.active:before {
	background-color: #DF0B13;
}
.progressbar-swz-wc li:after {
  content: "";
  position: absolute;
  width: 98%;
  height: 1px;
  background-color: #000;
  top: 15px;
  left: -50%;
  z-index : 1;
}
.progressbar-swz-wc li:first-child:after {
  content: none;
}
.progressbar-swz-wc li.active {
  color: #5a5a5a;
}
.progressbar-swz-wc li.active:before {
  border-color: #5a5a5a;
} 
.progressbar-swz-wc li.active + li:after {
  background-color: #000;
}
.progressBarContainer{
	width: 100%;    
    padding-top: 2%;    
    margin-left: 0.5%;
	float:left;
	margin-bottom:1%;
}
.progressbar-swz-wc{
	float:left;
	margin:0;
	width:100%;
	
}
button.crop-btn-type{
	color: #000 !important;
	background-color: #FFF!important;
	border: #7e7e7e;
	border-radius: 8px;
}
#Subheader{
	padding-bottom:20px;
}
.extra-spacing-block{
	display:none;
	color:#FFF;
	font-size:12px;
}
@media screen and (max-width: 723px) and (min-width: 300px) 
{
	.extra-spacing-block{
		display:block;
	}
}
@media screen and (max-width: 475px) and (min-width: 300px) 
{
	.extra-spacing-block{
		font-size:10px;
	}
}
.line2 {
    float: left;
    width: 66%;
    text-align: justify;
    color: #5a5a5a;
}
#zoom-slider,#brightness-slider,#contrast-slider {
    width: 90%;
    height: 1px;
    background-color: #5a5a5a;
    border: 0.5px solid #5a5a5a;
    float: left;
    margin-top: 15px;
    margin-left: 7px;
}
.line1
{
	border: 1px solid #5a5a5a;
    float: left;
    width: 35%;
    border-radius: 7px;
    height: 31px;
    text-align: center;
    padding-top: 4px;
    margin-top: 10px;
    font-size: 16px;
	color:#5a5a5a;
}

.line2
{
	float:left;
	width:66%;
	text-align:justify;
	color:#5a5a5a;
}
.line3
{
	color:#5a5a5a;
	font-size:18px;
}
.line4
{
	float:left;
	font-size:18px;
	margin-top:2px;
	cursor:pointer;
}
.line5
{
	float:left;
	font-size:18px;
	margin-top:5px;
	margin-left:4px;
	cursor:pointer;
}
.line6
{
	float: left;
    margin-top: 13px;
	font-size:16px;
	color:#5a5a5a;
}
.line7
{
	float: left;
    margin-top: 13px;
	font-size:16px;
	margin-left:6px;
	color:#5a5a5a
}
.line8
{
	margin-top: 13px;
    border: 1px solid #5a5a5a;
    color: #5a5a5a;
    height: 30px;
    padding-top: 3px;
    font-size: 17px;
    text-align: center;
    border-radius: 10px;
	font-family: "Lato", Arial, Tahoma, Sans-Serif;
	font-weight: bold;
}
.line8:hover
{
	background-color: #333333;
	color: white;
}
span.ui-slider-handle.ui-state-default.ui-corner-all

{
	height:10px;
	width:10px;
	border-radius:25px;
	left:50%;
	border:1px solid white;
	background:#ff0000;
	margin-top:-1px !important; 
	cursor:pointer;
}
input.box
{
	float: left;
    margin-top: 7px;
    width: 32%;
    height: 22px;
    margin-left: 10px;
    border-bottom: 1px solid #5a5a5a;
    border-top: none;
    border-left: none;
    border-right: none;
	padding-left:10px;
}
.a1{
	padding-left:0;
}
span.ui-selectmenu-button.ui-button {
    margin-top: 3px;
    margin-left: 10px;
    height: 35px;
    border-radius: 7px;
    width: 70%;
    border: 1px solid #5a5a5a;
    padding-left: 35px;
    background-color: #fff;
}
span.ui-selectmenu-button.ui-button:hover {
    background-color: #000!important;
    color: #FFF;
}
#resetAllOptions{
	cursor:pointer;
}
.message-box-elm{
	display:none;
}
.message-box-elm.active{
	display:block;
}
.wishlist-icon{
	font-size: 21px;
    margin-bottom: 0;
    margin-right: 3px;
    margin-top: 10px;
    position: absolute;
    right: 4%;
    z-index: 999;
    color: #f00;
	cursor:pointer;
}
.remove-element{
	display:none!important;
}
#showImageModal .modal-dialog {
      width: 100%;
      height: 100%;
      padding: 0;
      margin:0;
}
#showImageModal .modal-content {    
      height: 100%;
      border-radius: 0;
      color:white;
      overflow:auto;
	  background: #f1f1f15c;

}
#showImageModal .modal-content .modal-header button {
	color:#FFF!important;
}
#showImageModal .modal-content .modal-body{
	text-align:center;
}
span.ui-selectmenu-button.ui-button{
	margin-left:0;	
	padding-top:7px;
}
body{
	font-family:Lato;
}
#progressBlock{
    width: 100px;
	display:none;
    border: 1px solid #5a5a5a;
    float: left;
    border-radius: 100%;
    padding-top: 20px;
    padding-bottom: 16px;
	background:#cccccceb;
    font-weight:700;
    position: absolute;
    z-index: 9999;
    color: #5a5a5a;
    left: 39%;
    top: 40%;
}
#progressBlock.active{
	display:block;
}
body{
	font-family: Lato !important;
}
.image-editor-container,.modified-image-container{
	display:none;
}
.image-editor-container.active,.modified-image-container.active{
	display:block;
}

</style>
<div class="progressBarContainer">
	<ul class="progressbar-swz-wc">
        <li class="active step-1"><span class="extra-spacing-block">*</span><span>CUSTOMIZE</span></li>
        <li class="step-2"><span>MEDIUM</span></li>
        <li class="step-3"><span class="extra-spacing-block">*</span><span>SHIPPING</span></li>
        <li class="step-4"><span class="extra-spacing-block">*</span><span>BILLING</span></li>
        <li class="step-5"><span class="extra-spacing-block">*</span><span>CONFIRMATION</span></li>
    </ul>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<div class="modified-image-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<img src="" id="modified_image_url"/>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 image-editor-container active">
<div class="doka-container"><div class="image-container"></div></div>
</div>
<div class="col-lg-12">
	<img style="float:right;width:17px;margin-top:5px;margin-left:5px;"src="<?php echo site_url('/wp-content/uploads/wishlist-heart.png'); ?>" class="addToWishlist"/>
	<p class="cursor-pointer show-fullscreen text-right">View Fullscreen<img style="height:auto;width:auto;height: 12px;float: right;margin-top: 5px;margin-right:3px;" src="https://redtmultiples.com/wp-content/uploads/fullscreen.png"></p>		
</div>
<div id="progressBlock">
	<div class="col-lg-12 text-center" style="color:#f00">Creating</div>
	<div class="col-lg-12 text-center progress-counter">0%</div>
</div>
</div>
<div class="side-panel col-lg-6 col-md-6 col-sm-12 col-xs-12 mcb-wrap woocommerce customization-step-1 active">
	<div class="column margin-bottom-10">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1 remove-element">

		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 a1">
		<p class="line3">Zoom</p>
		</div>


		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
		<p class="line4 reduceValSlider" data-target="#zoom-slider">-</p><div id= "zoom-slider"></div><p class="line5 addValSlider" data-target="#zoom-slider">+</p>
		<input type="hidden"  id="zoomInputval"/>
		</div>

	</div>
	<h3 style="font-size:20px;font-weight:600;margin-top:0;">Cropping & Dimensions</h3>
	<div class="col-lg-12 a1" style="margin-bottom:5px;">
		<!--div class="col-lg-6 col-sm-12 col-xs-12 a1">
			<select id="default_crop" style="border-radius:8px;">
				<option value="">Select Dimensions</option>			
				<option data-w="42" data-h="59.4" value="rect1">42 x 59.4 cm (A2)</option>
				<option data-w="59.4" data-h="84" value="rect2">59.4 x 84 cm (A1)</option>
				<option data-w="84" data-h="118.8" value="rect3">84 x 118.8 cm (A0)</option>
				<option data-w="106" data-h="150" value="rect4">106 x 150 cm</option>
				<option data-w="150" data-h="212" value="rect5">150 x 212 cm</option>
				<option data-w="40" data-h="40" value="sq1">40 x 40 cm</option>
				<option data-w="60" data-h="60" value="sq2">60 x 60 cm</option>
				<option data-w="80" data-h="80" value="sq3">80 x 80 cm</option>
				<option data-w="100" data-h="100" value="sq4">100 x 100 cm</option>
				<option data-w="150" data-h="150" value="sq5">150 x 150 cm </option>
			</select>
		</div-->
		<div class="col-lg-7 col-sm-12 col-xs-12 a1">
			<?php if($attributes): ?>
			<select id="default_crop_preset">
				<option disabled selected>PRESET SIZES</option>
				<?php foreach($attributes as $attribute): ?>
				<option data-w="<?php echo $attribute['width']; ?>" data-h="<?php echo $attribute['height']; ?>" value="attribute"><?php echo $attribute['name']; ?></option>
				<?php endforeach; ?>
			</select>	
			<?php endif; ?>	
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1" style="margin-top:1%;">	
		
		<p>Drag and select directly on the image and</p>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1" style="margin-top:1%;">
			<div class="col-lg-4" style="padding:0;">				
				<button class="cropImage customized_homepage_button" type="button">CROP</button>
			</div>		
		</div>
	</div>	

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1" style="margin-top:3%;">	
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 a1">
		<p class="line6">Enter</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 a1">
			<p class="line6">Height</p>
			<input type="text" id="height_image" class="box" placeholder="" />
			<p class="line6">cm</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 a1" style="padding">
			<p class="line6">Width</p>
			<input type="text" id="width_image" class="box" placeholder="" />
			<p class="line6">cm</p>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 a1">
			<button class="setCropSize customized_homepage_button" type="button">Set</button>
		</div>

		<div id="coords">
			<input type="hidden" placeholder="x1" id="x1" />
			<input type="hidden" placeholder="x2" id="x2" />
			<input type="hidden" placeholder="y1" id="y1" />
			<input type="hidden" placeholder="y2" id="y2" />			
			<input type="hidden" placeholder="w" id="crop_w" />
			<input type="hidden" placeholder="h" id="crop_h" />
		</div>
		<input type="hidden" id="mincropwidth"/>
		<input type="hidden" id="mincropheight"/>
	</div>
	<h3 class="heading2 remove-element">EFFECTS</h3>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1 remove-element">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 a1">
			<p class="line3">Brightness</p>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 a1">
			<p class="line4 reduceValSlider" data-target="#brightness-slider">-</p>
			<div id = "brightness-slider"></div>
			<p class="line5 addValSlider" data-target="#brightness-slider">+</p>
			<input type="hidden"  id="brightnessInputval"/>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1 remove-element">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 a1">
			<p class="line3">Contrast</p>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 a1">
			<p class="line4 reduceValSlider" data-target="#contrast-slider">-</p>
			<div id = "contrast-slider"></div>
			<p class="line5 addValSlider" data-target="#contrast-slider">+</p>
			<input type="hidden"  id="contrastInputval"/>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a1">

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 a1">			
			<button id="resetAllOptions" class="customized_homepage_button" type="button">RESET</button>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 a1 remove-element">
			<p class="line8 cursor-pointer addToWishlist">Wishlist</p>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 a1">		
			<button id="buyNowBtn" class="customized_homepage_button" type="button">NEXT</button>
		</div>

	</div>
	
	</div>
</div>
<div id="customizeOptModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body">
        <p></p>
      </div>      
    </div>
  </div>
</div>
<div id="showImageModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
	</div>	
    <!-- Modal content-->
    <div class="modal-content">      
      <div class="modal-body">
        <img id="imgEle" src="<?php echo $attachment[0]; ?>"/>
      </div>      
    </div>
  </div>
</div>
<style>
.doka--button-action-confirm,.doka--button-action-reset,.doka--button-action-cancel,.doka--toolbar,.doka--crop-rotator{
	display:none!important;
}
.doka--image-gl canvas {
    background: #cccccc9c!important;
    border-color: #fff!important;
}
</style>
<script>
const swz_ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
const dokaTarget = document.querySelector('.doka-container .image-container');
 jQuery(document).ready(function($){
	 function getCropDim (a, b) {
         return (b == 0) ? a : getCropDim (b, a%b);
     }
	 setCropEditor();
	var imagePreview = $('#imgEle');	
	
		function setCropEditor(){
			const dokaTarget = document.querySelector('.doka-container .image-container');	
			var cropRatio = null;
			if($("#height_image").val().length > 0 && $("#width_image").val().length > 0){
				var widthImage = $("#width_image").val() * 38;
				var heightImage = $("#height_image").val() * 38;
					
			}
		   var doka = Doka.create(dokaTarget,{
				src: "<?php echo site_url('/wp-content/uploads/doka-product-images/'.$filename); ?>",
				utils: ['crop'],
				outputData: true,				
				onconfirm: function(output) {
					cropData = output.data.crop;
					imagePreview.attr('src', URL.createObjectURL(output.file));
					
					//$("#showImageModal").modal("show");	
					cropImageCallback(output,URL.createObjectURL(output.file));	
				}
		   });
		}
      $("#Content").on("click",".cropImage",function(){
		   $(".image-editor-container").addClass("active");
		   $(".modified-image-container").removeClass("active");
		   $(".doka--button-action-confirm").click();
	   });
   
	  $("#Content").on("click","#resetAllOptions",function(){	
		  $(".image-editor-container").addClass("active");
		  $(".modified-image-container").removeClass("active");
		  $("#height_image").val("");
		  $("#width_image").val("");
		  Doka.destroy(dokaTarget);
		  setCropEditor();		  	
		  //$(".doka--button-action-reset").trigger("click");
	   });
	   $(".setCropSize").click(function(){
		   Doka.destroy(dokaTarget);
		   setCropEditor();
	   });
	   $("#default_crop_preset" ).selectmenu();
	   $("#default_crop_preset").on('selectmenuchange', function() {
		   if($("#default_crop_preset").val() != null && $("#default_crop_preset").val().length > 0){
				var widthImage = $("#default_crop_preset option:selected").data('w');
				var heightImage = $("#default_crop_preset option:selected").data('h');
				$("#height_image").val(heightImage);
				$("#width_image").val(widthImage);
		   }
	   });
	   $("#buyNowBtn").click(function(){
			window.location = '<?php echo site_url("/medium-frame?p_id=".$productId); ?>';
		});
	   function cropImageCallback(imgdata,imgurl){
			if($("#width_image").val().length > 0 && $("#height_image").val().length > 0){
				imgdata.data.height_image = $("#height_image").val();
				imgdata.data.width_image = $("#width_image").val();
			}else{
				var imgWidth = imgdata.data.image.width * 0.026458333;
				var imgHeight = imgdata.data.image.height * 0.026458333;
				imgWidth = Math.ceil(imgWidth);
				imgHeight = Math.ceil(imgHeight);
				$("#width_image").val(parseInt(imgWidth));
				$("#height_image").val(parseInt(imgHeight));
			}
			var heightImage = $("#height_image").val();
			var widthImage = $("#width_image").val();
			if(heightImage.length > 0){
				heightImage = parseInt(heightImage);
				if(heightImage > 200){
					$("#customizeOptModal .modal-body p").html("We love large works but just need to make sure your crop has the integrity to print that size.  Contact us to find out..");
					$("#customizeOptModal").modal("show");
					return false;
				}
			}
			if(widthImage.length > 0){
				if(widthImage > 200){
					$("#customizeOptModal .modal-body p").html("We love large works but just need to make sure your crop has the integrity to print that size.  Contact us to find out..");
					$("#customizeOptModal").modal("show");
					return false;
				}
			}
			if(widthImage.length == 0 || heightImage.length == 0){
				$("#customizeOptModal .modal-body p").html("Please enter height and width");
				$("#customizeOptModal").modal("show");
				return false;
			}
			
			imgdata.data.product_id = <?php echo $productId; ?>;
			imgdata.data.action = 'swz_save_customized_updated_settings';
			imgdata.data.imageurl = imgurl;			 
			/* imgdata.data.file = imgdata.file; */ 
		    var postdata = imgdata.data;
		 /*   var data = {
			'action': 'swz_save_customized_updated_settings',
			'width_image': $("#width_image").val(),	
			'height_image': $("#height_image").val(),
			'product_id': <?php echo $productId; ?>,
			'imagedata':imgdata,
			
			'imageurl':imgurl
		   }; */
			
			
		   var formData = new FormData();
		   formData.append('cropped_image', imgdata.file);
		   formData.append('action', 'swz_save_customized_updated_settings');
		   formData.append('width_image', $("#width_image").val());
		   formData.append('height_image', $("#height_image").val());
		   formData.append('product_id', <?php echo $productId; ?>);
		   formData.append('crop_x', imgdata.data.crop.center.x);
		   formData.append('crop_y', imgdata.data.crop.center.y);
		   formData.append('zoom', imgdata.data.crop.zoom);
		   formData.append('crop_width', imgdata.data.image.width);
		   formData.append('crop_height', imgdata.data.image.height);
		   $("#progressBlock").addClass("active");
		   	var actualTime = 0;
			var croppingTimer = setInterval(function(){
				actualTime = parseInt(actualTime) + 5;			
				$(".progress-counter").html(actualTime+"%");
				if(actualTime >= 90){
					clearInterval(croppingTimer);
				}
				 
			},300);
		   
		   $.ajax({
				url: swz_ajaxurl,
				data: formData,
				method: 'POST',
				dataType: 'JSON',
				processData: false,
				contentType: false,
				success: function(response){					
					
				},
				complete: function(response){
					console.log(response.responseJSON);
					$(".image-editor-container").removeClass("active");
					$(".modified-image-container").addClass("active");
					$("#modified_image_url").attr("src",response.responseJSON.modified_image_url);
					$(".progress-counter").html("100%");
					$("#progressBlock").removeClass("active");
				}
			});
	   }
	   
	   $(".addToWishlist").click(function(){		
		$.get("<?php echo site_url('/?add_to_wishlist='.$productId); ?>",function(){
			$(".addToWishlist").addClass('active');
			
		});
		$(".show-fullscreen").click(function(){			
			$("#showImageModal").modal("show");
		});
	});
	   
   }); 

	
</script>


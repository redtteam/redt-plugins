<?php 
 $productId = $_REQUEST['p_id'];	
 $attachment_ids[0] = get_post_thumbnail_id( $productId );
 $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
 $product = wc_get_product( $productId );
 $productPrice = $product->get_price();


 $sizes = $product->get_variation_attributes();
 $attributes = array();
 if(isset($sizes['pa_select-size'])){
	$sizes = $sizes['pa_select-size'];
	foreach($sizes as $size){
		$newAttr = str_replace('-cm','',$size);
		$newAttr = explode('-x-',$newAttr);
		$saveAttr = array();
		if(isset($newAttr[0])){
			$saveAttr['width'] = str_replace('-','.',$newAttr[0]);
			$saveAttr['height'] = str_replace('-','.',$newAttr[1]);
		}
		$attributes[] = $saveAttr;
	}
 }
 
?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
.black-color{
	color:#000;
}
#coords{
	display:none;
}
#processing-bar{
	font-size: 19px;   
    color: #f00;
    position: absolute;
    top: 20%;
    left: 50%;
    background: #ccc;
    padding: 7px;
    font-weight: bold;
	display:none;
}
#imageBlock.processing #processing-bar{
	display:block;
}
#imageBlock.processing img{
	opacity:0.5;
}
/* #contrast-slider
{
	width:90%;
	height:3px;
	margin-top:10px;
	background-color:#333333;
	border:1px solid #333333;
	margin-left:16px;
}
#brightness-slider
{
	width:90%;
	height:3px;
	margin-top:10px;
	margin-left:16px;
	background-color:#333333;
	border:1px solid #333333;
	
} */
#contrast-slider a.ui-slider-handle.ui-state-default.ui-corner-all,#brightness-slider a.ui-slider-handle.ui-state-default.ui-corner-all
{
	height:17px;
	width:17px;
	border-radius:25px;
	left:50%;
	border:1px solid #fff;
	background:#000;
	margin-top:-3px !important; 
}
.zeroElement {
    float: right;
    
}
.zeroElement2 {
    float: right;   
}
.customization-step-1,.customization-step-2{
	display:none;
}
.customization-step-2.active,.customization-step-1.active{
	display:block;
}
.heading1{	
    font-weight: bolder;
    font-size: 20px;
}
.heading5,.heading6{
	margin-bottom: 0;
}
.side-panel {    
    padding: 10px;
    color: #5a5a5a;    
}
.btn-custom-swz {
    background-color: #e5e5e5;
    margin-top: 17px;    
    /*color: #fff !important;*/
    text-align: center;
    padding: 14px;
    border-radius: 35px;
    font-weight: bold;
    font-size: 15px;
    letter-spacing: 2px;
}
.margin-bottom-10{
	margin-bottom: 10px;
}
.reset-option {
    border-bottom: 1px solid red;
    padding-bottom: 1px;
    letter-spacing: 1px;
    margin-top: 22px;
    font-size: 13px;
    text-transform: uppercase;
    text-decoration: none!important;
    color: #FFF;
}
 .jcrop-holder img, img.jcrop-preview { max-width: none !important } 
 .cropImage.img{
	padding: 10px;
    border-radius: 100%;
    float: left;
    border: 1px solid #000;
    padding-top: 6px;
 }
 .cropImage.img img{
	width: 20px;
    margin-top: 4px;
 }
 .progressbar-swz-wc {
  counter-reset: step;
}
.progressbar-swz-wc li {
  list-style: none;
  display: inline-block;
  width: 17.33%;
  position: relative;
  text-align: center;
  cursor: pointer;
  color: #5a5a5a;
  font-family:Lato;
  font-size:12px;
  font-weight:700;
}
.progressbar-swz-wc li:before {
  content: "";  
  width: 5px;
  height: 5px;
  line-height : 30px;
  border: none;
 
  border-radius: 100%;
  display: block;
  text-align: center;
  margin: 13px auto 5px auto;
  background-color: #5a5a5a;
}
.progressbar-swz-wc li.active:before {
	background-color: #DF0B13;
}
.progressbar-swz-wc li:after {
  content: "";
  position: absolute;
  width: 98%;
  height: 1px;
  background-color: #000;
  top: 15px;
  left: -50%;
  z-index : 1;
}
.progressbar-swz-wc li:first-child:after {
  content: none;
}
.progressbar-swz-wc li.active {
  color: #5a5a5a;
}
.progressbar-swz-wc li.active:before {
  border-color: #5a5a5a;
} 
.progressbar-swz-wc li.active + li:after {
  background-color: #000;
}
.progressBarContainer{
	width: 100%;    
    padding-top: 2%;    
    margin-left: 0.5%;
	float:left;
	margin-bottom:4%;
}
.progressbar-swz-wc{
	float:left;
	margin:0;
	width:100%;
	
}
button.crop-btn-type{
	color: #000 !important;
	background-color: #FFF!important;
	border: #7e7e7e;
	border-radius: 8px;
}
#Subheader{
	padding-bottom:20px;
}
.extra-spacing-block{
	display:none;
	color:#FFF;
	font-size:12px;
}
@media screen and (max-width: 723px) and (min-width: 300px) 
{
	.extra-spacing-block{
		display:block;
	}
}
@media screen and (max-width: 475px) and (min-width: 300px) 
{
	.extra-spacing-block{
		font-size:10px;
	}
}
.line2 {
    float: left;
    width: 66%;
    text-align: justify;
    color: #5a5a5a;
}
#zoom-slider,#brightness-slider,#contrast-slider {
    width: 90%;
    height: 1px;
    background-color: #5a5a5a;
    border: 1px solid #5a5a5a;
    float: left;
    margin-top: 15px;
    margin-left: 7px;
}
.line1
{
	border: 1px solid #5a5a5a;
    float: left;
    width: 35%;
    border-radius: 7px;
    height: 31px;
    text-align: center;
    padding-top: 4px;
    margin-top: 10px;
    font-size: 16px;
	color:#5a5a5a;
}

.line2
{
	float:left;
	width:66%;
	text-align:justify;
	color:#5a5a5a;
}
.line3
{
	color:#5a5a5a;
	font-size:18px;
}
.line4
{
	float:left;
	font-size:18px;
	margin-top:2px;
	cursor:pointer;
}
.line5
{
	float:left;
	font-size:18px;
	margin-top:5px;
	margin-left:4px;
	cursor:pointer;
}
.line6
{
	float: left;
    margin-top: 13px;
	font-size:16px;
	color:#5a5a5a;
}
.line7
{
	float: left;
    margin-top: 13px;
	font-size:16px;
	margin-left:6px;
	color:#5a5a5a
}
.line8
{
	    margin-top: 13px;
    border: 1px solid #5a5a5a;
    color: #5a5a5a;
    height: 30px;
    padding-top: 3px;
    font-size: 17px;
    text-align: center;
    border-radius: 10px;
}
span.ui-slider-handle.ui-state-default.ui-corner-all

{
	height:17px;
	width:17px;
	border-radius:25px;
	left:50%;
	border:1px solid white;
	background:#ff0000;
	margin-top:-3px !important; 
	cursor:pointer;
}
input.box
{
	float: left;
    margin-top: 7px;
    width: 32%;
    height: 22px;
    margin-left: 10px;
    border-bottom: 1px solid #5a5a5a;
    border-top: none;
    border-left: none;
    border-right: none;
	padding-left:10px;
}
.a1{
	padding-left:0;
}
span.ui-selectmenu-button.ui-button {
    margin-top: 3px;
    margin-left: 0px;
    height: 35px;
    border-radius: 7px;
    width: 58%;
    border: 1px solid #5a5a5a;
    padding-left: 35px;
    background-color: #fff;
}
span.ui-selectmenu-button.ui-button:hover {
    background-color: #333333;
    color: white;
}
#resetAllOptions{
	cursor:pointer;
}
button.addTocart{
	border: 1px solid #7b7b7b!important;
    border-radius: 8px!important;
	margin-top: 12px;
}
.cursor-pointer{
	cursor:pointer;
}
label[data-frame="frame1"]{
	background: #1C1D18;
	width:21px;
	height:21px;
	margin-right:5px;
    margin-bottom: 0;
}
label[data-frame="frame2"]{
	background: #604237;
	width:21px;
	height:21px;
    margin-right: 5px;
    margin-bottom: 0;
}
.frame-selector.active{
	border: 1px solid #f00;
}
#default_frame_outer{
    margin-top:3%;
}
#default_frame_outer .col-lg-12 {
    padding: 0;
}

#default_frame_outer .col-lg-12 span {
    display: -webkit-box;
}

#default_frame_outer .col-lg-12 span > span {
    vertical-align: top;
}
body{
	font-family: Lato !important;
}
button.customized_homepage_button,a.customized_homepage_button,button.customized_homepage_button:hover,a.customized_homepage_button:hover{
    padding: 10px 30px;
    font-weight: bold;
    font-size: 11px;
    letter-spacing: 2px;
    background: #FFF!important;
   
    color: #000!important;
    border-radius: 19px;
    background-color: #FFF!important;
    border: 2px solid #000 !important;
    text-transform: uppercase;
	width:auto!important;
	text-deocration:none!important;
}
</style>
<?php
$productCustomized = array(); 
$modified_image = "";
$imageWidth = 0;
$imageHeight = 0;
if(isset($_SESSION['customizeProduct'][$productId])){
	$productCustomized = $_SESSION['customizeProduct'][$productId];
	
	$imageWidth = $productCustomized['width_image'];
	$imageHeight = $productCustomized['height_image'];
	if(!empty($productCustomized["modified_image_url"])){
		$modified_image = $productCustomized["modified_image_url"];
	}
}
/* not sure what it is. 
echo "<pre>";
print_r($productCustomized);
echo "</pre>";*/
?>
<div class="progressBarContainer">
	<ul class="progressbar-swz-wc">
        <li class="step-1"><span class="extra-spacing-block">*</span><span>CUSTOMISE</span></li>
        <li class="active step-2"><span>MEDIUM</span></li>
        <li class="step-3"><span class="extra-spacing-block">*</span><span>SHIPPING</span></li>
        <li class="step-4"><span class="extra-spacing-block">*</span><span>BILLING</span></li>
        <li class="step-5"><span class="extra-spacing-block">*</span><span>CONFIRMATION</span></li>
    </ul>
</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	<div id="imageBlock" class="column">
	<?php if($modified_image): ?>
		<img src="<?php echo $modified_image; ?>" />
	<?php else: ?>
		<img src="<?php echo $attachment[0]; ?>" />
	<?php endif; ?>	
	</div>
</div>
<div class="side-panel col-lg-6 col-md-6 col-sm-12 col-xs-12 mcb-wrap woocommerce customization-step-2 active">
    <div class="col-lg-12">
        <label>Medium</label>
    </div>
	<div class="col-lg-12">
		<select id="default_medium">
			<option value="paper">Paper</option>			
			<!--<option value="canvas">Canvas</option>	-->		
		</select>			
	</div>
	<input type="hidden" id="framePriceInput" />
	<input type="hidden" id="actualPriceInput" />
	<input type="hidden" id="default_frame" value="no"/>
	<!-- hide this for now. -->
<!-- 
	<div id="default_frame_outer" class="col-lg-12">
		<label>Frame</label>
		<p>Add premium framing for only $<span id="framePrice"></span>HKD</p>
		<input type="hidden" id="framePriceInput" />
		<input type="hidden" id="actualPriceInput" />
		<select id="default_frame">
			<option class="remove" value="yes">Yes</option>			
			<option value="no">No</option>			
		</select>
		<input type="hidden" id="default_frame" value="no"/>
		<div class="col-lg-12">
			<span><label class="frame-selector cursor-pointer" data-frame="frame1"></label><span>Painted black paint marupa wood with visible grain</span></span>
			<span style="margin-right: 5%;"><label class="frame-selector cursor-pointer" data-frame="frame2"></label><span>Painted natural varnish marupa wood with visible grain</span></span>
		</div>		
	</div>
	<div class="col-lg-12">
		<label style="width:100%;">Quantity</label>
		<input type="number" id="quantity" class="two-third column" placeholder="Quantity" value="1" />		
	</div>
	 -->
	 <input type="hidden" id="quantity" class="two-third column" placeholder="Quantity" value="1" />
	<div class="col-lg-12" style="font-weight:600;">Original Price: <?php echo $productPrice; ?></div>
	<div class="col-lg-12">
		<label id="actualPriceBlock"><label>
	</div>
	<div class="col-lg-12">
		<button class="addTocart customized_homepage_button alt" data-process="continue" style="text-transform: uppercase;">Add to cart and continue creating</button>
		<!--<button class="addTocart customized_homepage_button alt" data-process="checkout" style="text-transform: uppercase;">Checkout</button>-->
	</div>
</div>

<script>
var savedImageWidth = '<?php echo $imageWidth; ?>';
var savedImageHeight = '<?php echo $imageHeight; ?>';
const swz_ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
//const newajaxurl = "<?php echo site_url().'?swz_save_customized_settings_call=1'; ?>";
jQuery(document).ready(function($){
	//setCropSection();
	$("#resetEffects").click(function(){		
		$("#brightness-slider").slider("value",0);		
		$("#contrast-slider").slider("value",0);
	});
	$("#resetCrop").click(function(){
		$("#default_crop").val("");
		$("#width_image").val("");
		$("#height_image").val("");
		clearCoords();
		cropImageCallback();
		
	});
	$(".addTocart").click(function(){
		var processStatus = $(this).data('process');
		if(processStatus == 'checkout'){
			window.location = "<?php echo site_url('/checkout') ?>";
			return false;
		}
		var data = {
			'action': 'swz_saved_customized_add_to_cart',
			'product_id': <?php echo $productId; ?>,
			'frame': $('#default_frame').val(),
			'medium': $('#default_medium').val(),
			'quantity': $('#quantity').val(),
		}
		jQuery.post(swz_ajaxurl, data, function(response) {	
			// console.log(response); return false;	
			if(processStatus == 'cart'){
				window.location = "<?php echo site_url('/cart') ?>";			
			}
			window.location = "<?php echo site_url('/cart') ?>";			
		});
	});

	function showCoords(c)
	{
		var ratioW = $('#imageBlock img')[0].naturalWidth / $('#imageBlock img').width();
        var ratioH = $('#imageBlock img')[0].naturalHeight / $('#imageBlock img').height();
        var currentRatio = Math.min(ratioW, ratioH);
		$('#x1').val(c.x * currentRatio);
		$('#y1').val(c.y * currentRatio);
		$('#x2').val(c.x2);
		$('#y2').val(c.y2);
		$('#crop_w').val(c.w * currentRatio);
		$('#crop_h').val(c.h * currentRatio);
		var widthImageCm = (c.w * currentRatio) * 0.026458333;
		var heightImageCm = (c.h * currentRatio) * 0.026458333;
		$('#width_image').val(Math.ceil(widthImageCm));
		$('#height_image').val(Math.ceil(heightImageCm));
		console.log(currentRatio);
		console.log(c);
		
	}

	function clearCoords()
	{
		$('#coords input').val('');
	}
	function cropImageCallback(){
		$("#imageBlock").addClass("processing");
		var data = {
			'action': 'swz_save_customized_settings',
			'product_id': <?php echo $productId; ?>,
			'width_image': $("#width_image").val(),	
			'height_image': $("#height_image").val(),	
			'default_crop': $("#default_crop").val(),	
			'default_crop_preset': $("#default_crop_preset").val(),	
			'brightness': $("#brightnessInputval").val(),	
			'zoomlevel': $("#zoomInputval").val(),	
			'contrast': $("#contrastInputval").val(),
			'attr_width': $("#default_crop option:selected").attr('data-w'),	
			'attr_height': $("#default_crop option:selected").attr('data-h'),	'preset_attr_width': $("#default_crop_preset option:selected").attr('data-w'),	
			'preset_attr_height': $("#default_crop_preset option:selected").attr('data-h'),	
			'x1': $("#x1").val(),	
			'y1': $("#y1").val(),	
			'x2': $("#x2").val(),	
			'y2': $("#y2").val(),	
			'crop_w': $("#crop_w").val(),	
			'crop_h': $("#crop_h").val(),	
		};
		jQuery.post(swz_ajaxurl, data, function(response) {
			$("#imageBlock").removeClass('processing');
			if(response.image != ""){				
				$("#imageBlock").html('<img src="'+response.image+'"/><div id="processing-bar">Processing</div>');
				//setCropSection();				
			}
		},'json');
		
	}
	function defaultFrameChange(){
		var productPrice = $("#actualPriceInput").val();
		if($("#default_frame").val() != "no"){
			var framePrice = $("#framePriceInput").val();
			productPrice = parseFloat(productPrice) + parseFloat(framePrice);
			productPrice = productPrice.toFixed(2); 
		}
		$("#actualPriceBlock").html('Actual Price: '+productPrice);	
	}
	$("#default_medium").on('selectmenuchange', function() {
		var curMedium = $("#default_medium").val();
		var productPrice = $("#actualPriceInput").val();
		if(curMedium == 'canvas'){
			$("#default_frame").val("no");
			$("#default_frame_outer").hide();	
			$(".frame-selector").removeClass("active");	
		}else{
			$("#default_frame_outer").show();
			if($("#default_frame").val() != "no"){				
				var framePrice = $("#framePriceInput").val();
				productPrice = parseFloat(productPrice) + parseFloat(framePrice);
				productPrice = productPrice.toFixed(2);
			}	
		}
		$("#actualPriceBlock").html('Actual Price: '+productPrice);		
	});
	saveCropDataUser();
	function saveCropDataUser(){		
		var perCentage = 185;
		var framePrice = 1330;
		var pixelToCm = 0.02645833;
		var productPrice = <?php echo $productPrice; ?>;
		cropW = $('#imageBlock img')[0].naturalWidth * pixelToCm; 
		cropH = $('#imageBlock img')[0].naturalHeight * pixelToCm;
		cropW = parseInt(cropW);
		cropH = parseInt(cropH);
		if(savedImageWidth > 0){
			cropW = savedImageWidth;
		}
		if(savedImageHeight > 0){
			cropH = savedImageHeight;
		}
		console.log(cropW);	
		console.log(cropH);			
		console.log(savedImageHeight);			
		console.log(savedImageWidth);			
		
			if(cropW > 100 && cropH > 100){
				perCentage = 110;
			}
			if(cropW <= 100 && cropH <= 100){
				if(cropW > 84 || cropH > 60){
					perCentage = 120;
				}
			}
			if(cropW <= 84 && cropH <= 60){
				if(cropW > 42 || cropH <= 60){
					perCentage = 145;
				}
			}
			if(cropW <= 42 && cropH <= 60){
				if(cropW <= 42 || cropH >= 30){
					perCentage = 160;
				}
			}
			if(cropW <= 42 && cropH <= 30){
				if(cropW >= 21 || cropH >= 29.7){
					perCentage = 185;
				}
			}
			if(cropW <= 21 && cropH <= 29.7){
				perCentage = 185;
			}
			var actualPrice = productPrice;
			/* productPrice = (cropW * cropH) * 0.135 * (perCentage/100);
			
			productPrice = parseFloat(productPrice) * (perCentage/100);
			
			productPrice = parseFloat(actualPrice) + parseFloat(productPrice); */
			console.log(actualPrice);
			console.log(perCentage);
			productPrice = parseFloat(actualPrice) + (((cropW * cropH) * 0.135 *(perCentage/100)) * (perCentage/100));
			if(cropW <= 120){
				framePrice = 5480;
			}
			if(cropW < 106){
				framePrice = 4270;
			}
			if(cropW < 84){
				framePrice = 3020;
			}
			if(cropW < 59.4){
				framePrice = 1880;
			}
			if(cropW <= 42){
				framePrice = 1330;
			}
			if(cropW > 120){
				
			}
			$("#actualPriceInput").val(productPrice.toFixed(2));
			
				
		$("#actualPriceBlock").html('Price: '+productPrice.toFixed(2));
	
		$("#framePrice").html(framePrice);
		$("#framePriceInput").val(framePrice);		
		
	}
	$("#default_crop").change(function(){
		$("#default_crop_preset").val("");
		cropImageCallback();
		//setCropSection();
		
	});
	$("#default_crop_preset").change(function(){
		$("#default_crop").val("");
		cropImageCallback();		
	});
	$(".cropImage").click(function(){
		cropImageCallback();

	});
	$( "#zoom-slider" ).slider(
		{
			value: 100,
			min: 0,
			max: 200,
			change: function(event,ui){
				//$("#zoom-val").html(ui.value);
				$("#zoomInputval").val(ui.value);
				cropImageCallback();
			}
			
		}
	);
	$( "#brightness-slider" ).slider(
		{
			value: 0,
			min: -100,
			max: 100,
			change: function(event,ui){
				$("#brightness-val").html(ui.value);
				$("#brightnessInputval").val(ui.value);
				cropImageCallback();
			}
			
		}
	);
	$( "#contrast-slider" ).slider(
		{
			value: 0,
			min: -100,
			max: 100,
			change: function(event,ui){				
				$("#contrast-val").html(ui.value);
				$("#contrastInputval").val(ui.value);
				cropImageCallback();
			}
			
		}
	);
	$( "#default_crop_preset" ).selectmenu();
	$(".reduceValSlider").click(function(){
		const curTarget = $(this).data('target');
		var value = $(curTarget).slider("value");
		var step = $(curTarget).slider("option", "step");
		$(curTarget).slider("value", value - step);
	});
	$(".addValSlider").click(function(){
		const curTarget = $(this).data('target');
		var value = $(curTarget).slider("value");
		
		var step = $(curTarget).slider("option", "step");
		$(curTarget).slider("value", parseInt(value) + parseInt(step));
	});
	$("#resetAllOptions").click(function(){
		$("#default_crop").val("");
		$("#default_crop_preset").val("");
		$("#width_image").val("");
		$("#height_image").val("");
		$("#zoomlevel").val("");
		$("#brightness-slider").slider("value",0);		
		$("#contrast-slider").slider("value",0);
		clearCoords();
		cropImageCallback();		
	});
	$(".frame-selector").click(function(){
		var currentFrame = $(this).data("frame");		
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$("#default_frame").val("no");
		}else{
			$(".frame-selector").removeClass("active");
			$(this).addClass("active");
			$("#default_frame").val(currentFrame);
		}			
		defaultFrameChange();
	});
    $("#default_medium").selectmenu();
});
</script>